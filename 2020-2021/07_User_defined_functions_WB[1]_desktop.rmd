---
title: "7 User defined functions and the apply family"
author: "Kristijan Čeple"
date: "18. November 2020"
output:
  html_document: default
---

<!--  DO NOT FORGET TO PUT YOUR NAME AND CORRECT DATE IN THE ABOVE PREAMBLE! -->

## Leading Programming paradigms


- **Object-oriented programming** represents a subcategory of the **imperative paradigm**, which has the programmer use a series of statements (commands) that are processed sequentially (and sometimes conditionally). In general, the programmer's task is to describe in detail *how* a certain task should be done. With the object-oriented approach the program is seen as a system of **nouns** where the components are realized in the form of objects that encapsulate the attributes of the mentioned noun and the methods that perform certain tasks related to the given noun. Also, object-oriented systems focus on the **controlled change of the components' state**, commonly as a result of the exchange of messages between them. 

- **Functional programming** is a subcategory of **declarative paradigm** where the programmer describes *what* needs to be done, without necessarily specifying how. Functional approach sees the program as a system of **verbs** where the functions, i.e. the tasks we want to execute, have priority over the components over which these tasks are executed. Functional programming models the information system through **components that generally do not change their own state**, so that the result of the program is strictly dependent on the inputs, which facilitates testing and maintenance.

R readily supports both paradigms, but a shift towards the functional programming paradigm might be more in tune with the "R way" of solving problems. 

In R, the following is true: *functions are "first-order" objects*, we can *reference them with a variable* of the selected environment, *send them to functions* as arguments, *receive as return values* function and *store them in data structures*, such as a list. The function in R is simply an "executable" object. A large number of functions - especially those that replace the program loop construct - work on the principle of functional languages where we perform the work in a manner that declaratively specifies which function we want to apply on which data structure, and let the programming language itself perform low-level tasks such as iteration by structure and preparation of the result. Examples of this will be learned soon, but now let's first learn how to create our own functions in R.




## Defining a new function

In a general case, the definition of a new function looks like this:

```{r, eval = F}
function_name <- function(input arguments) {
    function_body
    return_statement
}
```


Let's try to create a simple function. For starters, let's make our own implementation of the `abs` function, called `myAbs`.

## Exercise 7.1 - first user defined function


```{r}
# create a function called `myAbs` which
# will mimic R's `abs` function (but will not call it directly)
myAbs <- function(number) {
  if(number >= 0) {
    return number
  } else {
    return -number
  }
}

myAbs(5)
myAbs(5)
myAbs(5)
myAbs(5)
```

## Creating robust funcitons

If we want to increase the robustness of the function in such a way that we reject the execution of the logic within a function if certain conditions are not satisfied, we can use the `stopifnot` function. This function calculates the default logical expression and terminates the function if the specified condition *is not true*.



## Exercise 7.2 - using 'stopifnot' inside a function definition


```{r}
# write the function `parallelMax` which requires two numeric vectors as input
# and returns a vector of the same size containing the larger 
# between two corresponding elements of the original vectors
# if one or both vectors aren't numeric or aren't the same size
# the function must throw an error
# do not use loops!

# execute this new function over the following vector pairs
# c(T, F, T) i c(1, 2, 3)
# c(1, 2, 3, 4) i c(5, 6, 7)
# c(1, 2, 3) i c(0, 4, 2)

# (second part of the exercise should be tested inside the console!)

```



## Named and unnamed parameters

When calling a function, we can optionally specify the parameter names, and R will even allow the mixing of named and unnamed parameters (although this is not something we should often use in practice). When R connects the sent values with formal parameters, the named parameters will have priority and will be resolved first, after which the unnamed parameters will be resolved in te order they were provided.

We can see this in the next exercise, in which we will use the opportunity to showcase a very useful function - `paste`. This function concatenates character strings with the addition of a space separator (there is an alternative function `paste0` for joining without spaces).



## Exercise 7.3 - function parameters


```{r, eval = F}
printABC <- function(a, b, c) {
   print(paste("A:", a, "B:", b, "C:", c))   
}

# think before executing - what will be printed with the following command? 
#printABC(1, a = 2, 3)
```




## Arbitrary number of parameters

If we want to write a function that receives an arbitrary number of arguments, we can use the element `...`, i.e. the ellipsis. An example of this feature is the built-in `paste` function which can receive an arbitrary number of character strings. If we use the ellipsis in our functions, we should place them at the end of the list of input arguments, and within the function itself we simply convert it into a list, and then access its parameters in the way that suits us.


## Exercise 7.4 - function with an arbitrary number of parameters


```{r}
printParams <- function(...) {
   params <- list(...)
   for (p in params) print(p)
}

# call the above function with any random parameters


```




### The "copy-on-modify" principle 

One of the more common questions raised when learning a new programming language is whether the functions work in "call-by-value" or "call-by-reference" mode. The difference is basically whether the function may change the content of the variables sent at the place of the formal argument or not;  *call-by-value* principle forward only copies of original arguments. On the other hand, the *call-by-reference* principle makes it so the function receives "references" of the original variables, i.e. it behaves as if the original variables were passed to the function and all changes to them would be reflected in the calling function or program.

The language R uses a hybrid principle known as *copy-on-modify*. With this principle, references are forwarded to the function, which allows us to transmit "large" variables without the fear of them getting unnecessarily copied. But this is only valid if the function does not change the value of the resulting variables - at the moment when the function attempts to make any changes, copying the variable is carried out and the function continues to work on the copy. 

Let's check the above statements in a following examples.



## Example 7.1 - "copy-on-modify"

```{r}
# creating a data frame
df <- data.frame(id = 1:5, name = LETTERS[1:5])

# function changes a column and returns nothing
# (assignment operator does not have a return value)
f <- function(x) {
    x$name[1] <- "change!"
}

# the original data frame is unchanged
f(df)
df


```



## Example 7.2 - "copy-on-modify (2)"

```{r}
# creating a data frame
df <- data.frame(id = 1:5, name = LETTERS[1:5])

# function changes a column of the external data frame and returns nothing
f <- function() {
    df$name[1] <- "change!"
}

# the original data frame is unchanged
f()
df


```



## Example 7.3 - "copy-on-modify" and environments


```{r}
# creating a data frame in a new environment
e <- new.env()
parent.env(e) <- emptyenv()
e$df <- data.frame(id = 1:5, name = LETTERS[1:5])

# function changes a data frame using a reference from the environment
f <- function(e) {
    e$df$name[1] <- "change!"
}


f(e)
e$df

```


## The `<<-` operator


A simpler way of solving the above task would be using the `<<-` operator. This operator's function is to change the variable of the given name that is located somewhere in the search path. R will follow the search path, and change the first occurrence of the specified variable. If the variable of this name does not exist anywhere in the search path, R will create a new variable in the first environment above the environment of the function.


## Example 7.4 - the `<<-` operator

```{r}
# operator `<<-`
f <- function(x) {
    x <<- 7
    x <- 6
}

x <- 5
f()
x
```

Let's see how this would work with our data frame.

## Example 7.5 - changing a data frame column with the `<<-` operator

```{r}
# creating a data frame
df <- data.frame(id = 1:5, name = LETTERS[1:5])

f <- function() {
    df$name[1] <<- "change!"
}

f()
df
```



## Lazy evaluation

Finally, we must mention one feature of functions in R - the so-called. "lazy evaluation". This simply means that R will not evaluate the received parameter until it is explicitly used. Up to that moment, this object is so-called. "promise" - R "knows" how to evaluate that object but it will not do so until it really needs it. This increases the efficiency of the language; if a parameter is used only in a conditional branch, then in scenarios when it is not needed it will not consume memory. But equally, we need to be careful because a lazy evaluation can lead to unexpected problems if we do not take into account its existence.


## Function as an object

We have already stated that in R functions are "first class objects". Let's explain this in more detail.

It is best to start with a trivial example. We know that R offers the function `sum` within the `base` package, and this function calculates the arithmetic sum of the vector elements we send to it as an input parameter. But `sum` is actually *the name of the variable* that references the code that implements this function. If we want, we can very easily bind this function to some other variable by which we have effectively changed its name, or - better said - provided an alternative way of calling from a completely different environment.


```{r, eval = F}
sum2 <- sum

sum2(1:10)  # same as sum(1:10)

```


This is easiest to understand in a way that the function is simply an "callable variable", whereby the "call" refers to the use of a syntax that includes a reference to the function and input arguments framed in parentheses, which will return some value after execution in the R environment.

## Function as a return value

The function can also be a return value from another function.

```{r}
funcCreator <- function() {
    f <- function(x) x + 1
    f
}

newFunc <- funcCreator() # we get the "add one" function
newFunc(5)  
```


The function simply creates a new function and returns it to the calling program as it would have done with any other object. The return value is stored in the variable that is now "callable" - if we add brackets and parameters it will be executed in the way it is defined within the function that created it.

Note that we could use the fact that the function returns the result of the last expression and define the function even shorter:

```{r}
# shorter definition
funcCreator <- function() {
    function(x) x + 1
}
```

These functions are often referred to as "factories" or "generators" of functions, and in contrast to the above example, in practice, the function generator often receives some parameters that determine how the returned function will behave.

Try to create a function factory that returns the multiplication functions using a pre-set parameter.



## Exercise 7.5 - function factory


```{r}
# create the `multiplicationFactory` function that creates 
# multiplication functions by the pre-set constant

# use the above function to create the `times2` function
# which doubles the received number

# call the `times2` function with parameter 3 and print out the result

```



The `multiplicationFactory` function actually creates a "family" of functions that all provide the multiplication option with the selected number - i.e. parameter selected by the programmer itself. This way of managing functions may be initially confusing, but by using it in practice (which we will show in the next chapter), it is easy to notice the added flexibility and effectiveness of such an approach.

## Anonymous functions

If we define a function, and do not bind it to some variable, then we created the so-called "anonymous function".

```{r, eval = F}
# anonymous function
function(x) x * x 
```


We can notice that each function is initially "anonymous". If we return to the function definition syntax, we see that it is actually a combination of creating an anonymous function and binding it to a new variable. Of course, leaving the function anonymous as we did in the example above does not make much sense, just as it does not make sense to define a vector or list without creating a reference to that object - in that case the created object is not in any way usable because there are no links to it and will be quickly deleted by R within the "garbage collection" routine.

We can ask ourselves - is there a scenario where the anonymous function is meaningful and useful? Explicit anonymous functions are used when a "disposable" function is needed, for example, as an argument for some other function. If the function we want to send as an argument is easy to define in a single line, and we do not plan to use it afterwards in the program, then it makes no sense to define it separately and assign it its own reference. An example of this will be seen in the `apply` family of functions.


## Final word on user defined functions

At the end of this section, we repeat the most important things - in R, the function is an object like any other, the only specificity is that it is an object that is "executable", ie, which, using the function call syntax, does some work and returns some value. Even anonymous function can be executed (although only once, since we do not have a reference for future calls).

```{r}
# calling the anonymous function
(function(x) x + 1)(2)
```





## What are `apply` functions?

Very often, knowledge of the basics of the language R is reflected by the skill of using the so-called `apply` family of functions, available in the` base` package. These functions are specifically designed to perform repetitive tasks over various data structures, and as such they replace the program logic that would usually be realized in a through program loops. Additionally, these functions typically receive other functions as input arguments and, to a certain extent, encourage the functional programming paradigm.

The family name comes from the fact that these functions commonly have a suffix "apply". Some of the functions from this family are:

- `apply`
- `lapply`
- `sapply`
- `vapply`
- `tapply`,` mapply`, `rapply` ...

All of these functions work in a similar way - they receive a data set, a function that we want to apply to elements of that set, and optional additional parameters, and as the output give a set of function results, most often "packaged" in an appropriate format. The difference is mainly in the types of input and output arguments, as well as specific details about the implementation of the function itself and/or the way results are prepared.

This family of functions is best learned through examples. We will begin with the "basic" function - `apply`.


## The `apply` function

The `apply` function is the only one that literally shares the name with the family of these functions. It is intended to work with **matrices** (actually with arrays, but since it is relatively rare to use data structures that have more than two dimensions, here we will focus only on matrices).

The command syntax is as follows:

```{r, eval = F}
result <- apply( <matrix>, <rows (1) or columns (2)>, <function_name> )
```

Or, described in words, to implement the `apply` function, we:

- choose a matrix
- decide whether to "cut it" by rows or columns
- declare which function we want applied to each row (or column)

Depending on how function works, as a result we get a matrix or (which is a more frequent case) a vector.

Let's try to use this function in a concrete example.



## Exercise 7.6 - the 'apply' function


```{r}
m <- matrix(1:9, nrow = 3, ncol = 3, byrow = TRUE)

# print matrix `m`

# use the `apply` function to calculate
# and print the column sums of the `m` matrix


# use the `apply` function to calculate
# and print the multiplicaton of row elements 
# from the `m` matrix 

```


## Function 'apply' and anonymous functions


If we want to perform a custom task to the rows/columns, we often use an anonymous function, for example:

```{r, eval = F}
apply(m, 1, function(x) x[1])   # return the first element of each row
```



## Exercise 7.7 - the 'apply' function and anonymous functions

```{r}
# for each row of `m` calculate the natural logarithm
# of the sum of row elements
# rounded to 2 decimals
# use the `apply` function

```



## Mechanics of the 'apply' function

Let's repeat - `apply` (and related functions) implicitly *disassembles the input data structure into elements*. In the examples above, these elements - rows or columns - are actually numeric vectors. The argument `x` received by an anonymous function is exactly that vector, or, better said *each of these vectors* that are sent one by one. *The results of the function are "remembered" and "packed"* into the final result.

Let's try to program the last example without using the `apply` function.



## Exercise 7.8 - Loop as an alterntive to the 'apply' function


```{r}
# for each row of `m` calculate the natural logarithm
# of the sum of row elements
# rounded to 2 decimals
# use the for program loop

```



## The 'apply' function and anonymous functions with multiple parameters

What if we want to send to the `apply` function a function which needs several parameters? For example, let's say that instead of the upper function that extracts the first line element we want a function with two parameters - the first a vector (matrix row or column), and the second an integer that indicates the index of the element to extract. The answer is simple - just add additional parameters at the end of the function call.

```{r, eval = F}
# apply function and input function with multiple parameters
apply(m, 1, function(x,y) x[y], 2)  # second element of each row
```

## Alternative helper functions for matrices

Finally, it should be noted that for similar processing of data in the matrix form, we do not necessarily need to use `apply` - many popular operations such as adding row or column elements, calculating the average of the elements of the rows and columns, and the like. This has already been implemented through functions such as `rowSums`,` colSums`, `rowMeans`,` colMeans` and the like. They are easier to use, but specialized - for more flexibility, the most common option is `apply`.




## The 'lapply', 'sapply' and 'vapply' functions

The name of the `lapply` function comes from *list apply* - i.e. **apply the function to the elements of lists**. To put simply - it is a function that will receive the **list and a function** as the input arguments, apply the functions to **each individual list element** and return again **a new list** as a result.



## Exercise 7.9 - the 'lapply' function

```{r}
l <- list(a = 1:3, b = rep(c(T, F), 10), c = LETTERS)

# use the `lapply` function to calculate the length (number of elements)
# of each element of the `l` list

```





## Exercise 7.10 - the 'lapply' function and anonymous functions



```{r}
# process the elements of the `l 'list as follows:

# - Calculate the mean value if it is a numerical vector
# - count the values of TRUE if it is a logical vector
# - calculate the length of the vector for all other cases

# use the `lapply` function and an anonymous function

# do not forget that anonymous function can also use blocks!
```



## The 'unlist' function

The `lapply` function is essentially quite simple to use and is very popular due to this fact. But once we use it for a while, we can find it irritating that t it always returns the list as a result, although some other data structure would be more suitable for us -  for example a vector, especially if the resulting list has just simple numbers as elements. For this reason, R offers the `unlist` function to simplify the list to a vector.



## Exercise 7.11 - the 'unlist' function 


```{r}

l <- list(a = 1:10, b = 10:20, c = 100:200)

# calculate the mean value of the elements of the `l` list
# print the results as a numeric vector
# use `lapply` and `unlist`
```



## 'sapply' - "simplified lapply"

The displayed combination of `lapply` and` unlist` will give us as a result a one-dimensional vector, which in many cases is what we want. But sometimes some other data structure would suit us - for example, a matrix. In this case we need an additional step in transforming a one-dimensional vector into a matrix using the `matrix` function, with the number of rows and columns being explicitly assigned.

The question may arise - why is `lapply` not able to check the structure of the result it has created and determine the optimal data structure for formatting it (vector, matrix, or list)? That's exactly the idea behind the `sapply` function, or *simplified list apply*. This function first performs `lapply` internally, and then simplifies the result to a vector, matrix or array, depending on the characteristics of the results obtained.

## Exercise 7.12 - the 'sapply' function 

```{r}

l <- list(a = 1:10, b = 10:20, c = 100:200)

# calculate the median of elements of the `l` list
# and collect the results in a numeric vector
# use the `sapply` function


# extract the first and last element of each of the elements of the `l` list
# use `sapply` and anonymous function

```




Note that as a result in the last example, we received a matrix, but that R formed it "by columns". If we wanted a matrix with elements arranged in rows, we can not use `sapply` for this directly, because the matrix is formed internally, without the possibility of forwarding the` byrow = T` parameter. To obtain such a matrix, one option is already mentioned with the combination of `lapply`,` unlist` and `matrix`, or - more simply - transposing the` sapply` results using `t` function (from *transpose*).

## The 'vapply' function

The `sapply` function is quite popular due to its simplicity and efficiency, so it is relatively often used in interactive analysis. On the other hand, the use of this function in program scripts is not recommended since its result is unpredictable in the general case - e.g. the script can expect a matrix in the continuation of the code, and the `sapply` function, due to the specificity of the input data, returns the vector, which can cause unforeseen results, which is not easy to spot later and diagnose where the error occurred.

If we are developing our own programs in R and want to use `sapply`, then the better choice will be the` vapply` function, which works identically to `sapply`, but uses an additional parameter called `FUN.VALUE` with which we explicitly define what kind of "simplification" we expect. For example. `numeric(3)` means that the result of applying the function to each element of the original list should be a numeric vector of three elements. If the result for any list item differs from the expected one, the function will raise an error.


## Exercise 7.13 - the 'vapply' function 


```{r, eval = F}
myList <- list(numbers <- c(1:5), 
               names <- c("Ivo", "Pero", "Ana"), 
               alphabet <- LETTERS)


# think which of the following calls will be successful,
# and which will throw out the error
# check the results on the console

vapply(myList, length, FUN.VALUE = numeric(1))
vapply(myList, function(x) as.character(c(x[1], x[2])), FUN.VALUE = character(2))
vapply(myList, function(x) as.logical(x), FUN.VALUE = character(1))

```


## The 'lapply' function and data frames


Finally, let's return briefly to `lapply` and consider one important fact - it is intended for use on lists, and **data frames are actually lists**. In other words, the `lapply` function is very handy for processing tabular datasets when we want to apply a particular function to the columns of the data frame.

One of the more frequent operations performed in data analysis is the so-called. "normalization" of the numeric columns of the data frame - i.e. reducing all numerical values to "normal" distribution with the arithmetic mean of 0 and standard deviation of 1. This can be done by reducing each individual value by the arithmetic mean of the column (the `mean` function) and dividing with standard deviation of the column (function `sd`). This is a great way to demonstrate the use of `lapply` with data frames.




## Exercise 7.14 - the 'lapply' function and data frames

```{r}
df <- data.frame( a = 1:10, b = seq(100, 550, 50), 
                  c = LETTERS[1:10], d = rep(c(T,F), 5), 
                  e = -10:-1)

# normalize numerical columns using `lapply`
# do not change the remaining columns
# round the normalized values to three decimal places
# save the result in the df variable

# print df

```



## Quick data frame conversion


We see that after using `lapply` we get a list and that if we want the result in the form of a data frame we need to add another step using the` as.data.frame` function. If we are looking for a simpler way that immediately gives the data frame as a result, there is one convenient "trick" that we will explain below.

Let's look at the solution of the previous problem, and put the following little change in the assignment of the result of the `lapply` function:

```{r, eval = F}
df[] <- lapply(...)
```


In this way, R will not create a "new" variable named `df`, but rather the` lapply` result will be entered in the 'all rows and columns of the `df` data frame. This made it possible for us to get the result in the form of a data frame, which we actually wanted. For this very reason, in R scripts, we will often see a similar syntax (`df [] <- lapply ...`). Try to modify the above example in the above manner and make sure that the result will be a data frame.

## Using 'sapply' to quickly examine column classes

Another commonly used trick in working with data frames is the following command:

```{r, eval = F}
sapply(df, class)   
```

This command actually gives us the answer to the question - which types are the columns of the given data frame? Although there are other ways to get this information, this method is popular both because of the compactness of the results and the independence of additional packages.


## Other functions from the `apply` family and the available alternatives

In the previous chapters, we have probably listed the most popular members of the `apply` family. This family has more members, including some who do not have a suffix *-apply*:

- `mapply`, which works in parallel over multiple data structures
- `rapply`, which recursively applies functions within the structure
- `tapply`, which applies functions over sub-groups within a structure defined by factors
- `Map`, the` mapply` version, which does not simplify the result
- `by`, the` tapply` version for data frames
- etc.

The reason why these functions will not be explained in detail is twofold: firstly, as already mentioned, these functions are in practice applied much less often than the functions we have shown in the previous chapters. Secondly, with the increase in popularity of the language R, a large number of packages are oriented to improve the existing functions of the language R in the sense of easier and more efficient programming, especially when working with data frames.

## Packages offering alternatives to the 'apply' family

If we are looking for convenient alternative functions to those from the `apply` family, it's recommended to look at some of the following packages

- `plyr` - an extremely popular package that, among other things, offers a number of functions very similar to `apply` functions, but derived in a way that they have a consistent signature and explicitly defined input and output formats that are easily read from the function name itself (in particular, the first letters ); so the `llply` function uses a list as both the input and the output, while` mdply` needs a matrix as input and outputs a data frame
- `purrr` - a package that replaces the functions of the` apply` family with functions corresponding to similar functions from other programming languages; since the application of the same function to a number of elements of a data structure in functional languages is often called "mapping", the set of functions of this package carries the prefix `maps_`, and the function names often correspond to the expected results (for example` map2_lgl` means that as a result we expect a logical vector , and the `map2_df` a data frame)
- `dplyr` - a relatively new package, which in a certain sense represents the successor of the `plyr` package but oriented almost exclusively toward data frames; the functions of this package are not so much oriented to replace the `apply` family functions as providing a specific platform for working with data frames in a manner similar to languages oriented precisely for this purpose, such as, for example, the SQL language

In future lectures we will introduce the `dplyr` package precisely because this package greatly facilitates and accelerates the data analysis process and is extremely well accepted in the R community.



## Homework exercises {-}

1. R has a `which` function which converts a logical vector into a numeric one containing indexes where the original vector has a `TRUE` value (so `c(T, F, F, F, F, T, F, T)` becomes `c(1, 6, 8)`). Create a function which replicates this behaviour. 


2. Take the numerical vector `x` of length `n`. In statistics, the standardized moment of the k-th order is calculated like this:

$$\frac{1}{n}\sum_{i=1}^n{(x_i - \bar{x})}^{k+1}$$


Create a factory of moment functions (`moment(k)`) for calculating the standardized central moment of the k-th order. 

Create the functions `zero_moment (x)` and `first_moment (x)` with parameter values `k` set to ` 0` and `1` accordingly. 

Test the functions on vector `1:1000`. Compare the results given by the `sd` (standard deviation) function over the vector `1: 1000` and root of the first moment you have calculated.


3. Take the `m` matrix created by the following command:

```{r}
m <- rbind(1:5, seq(2, 10, 2), rep(3, 5), 3:7, seq(100, 500, 100))
```

With the `apply` function and the new anonymous function, create a vector that will contain the first even element of each row, or zero if the corresponding row does not have even elements.


4. The following commands will create a list of 100 elements where each element will be a numeric vector of a random length of 1 to 10.

```{r}
set.seed(1234)
myList <- replicate(100, sample(1:10, sample(1:10, 1)))

```


With the help of `lapply` / `sapply` (and additional commands if necessary), create:

- the numerical vector `v` with the lengths of the list elements
- list `l` with normalized numerical vectors of the original list
- numerical vector `ind4` with indexes of all list elements containing number 4
- the `df5` data frame containing columns which have all the elements of the length of 5 from the original list



