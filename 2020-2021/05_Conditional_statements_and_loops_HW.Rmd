---
title: "05_Conditional_statements_and_loops_HW"
author: "Kristijan Ceple"
date: "5 11 2020"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

## R Markdown

This is an R Markdown document. Markdown is a simple formatting syntax for authoring HTML, PDF, and MS Word documents. For more details on using R Markdown see <http://rmarkdown.rstudio.com>.

When you click the **Knit** button a document will be generated that includes both content as well as the output of any embedded R code chunks within the document. You can embed an R code chunk like this:

```{r cars}
summary(cars)
```

## Including Plots

You can also embed plots, for example:

```{r pressure, echo=FALSE}
plot(pressure)
```

Note that the `echo = FALSE` parameter was added to the code chunk to prevent printing of the R code that generated the plot.

## Homework exercises {-}

# Assignment 1
1. Create a data frame 'cities'.
Add to it a column "taxLevel" which will be the ordinal factor variable with the levels "small", "medium" and "high" depending on whether the percentage of tax is strictly smaller than 12, between 12 and 15 or strictly greater than 15. Use the `ifelse` command.

```{r}
cities <- data.frame(zipcode = c(10000, 51000, 21000, 31000, 2000),
         cityName = c("Zagreb", "Rijeka", "Split", "Osijek", "Dubrovnik"),
         cityTax= c(18, 15, 10, 13, 10))
cities

# [12,15] = Medium
cities$taxLevel <- ifelse(cities$cityTax < 12, "small", ifelse(cities$cityTax <= 15, "medium", "high"))
cities$taxLevel <- factor(cities$taxLevel, level=c("small", "medium", "high"), ordered=T)
cities
cities$taxLevel
```

## Assignment 2
2. Replace the loops in the next block with equivalent vectorized operations (for the second loop, review the `sum` function documentation).

```{r, eval = F}
a <- numeric()
i <- 1

# Forms a [1,2,3,...,99,100] vector
while (i <= 100) {
  a <- c(a, i)
  i <- i + 1
}


total <- 0
# Aggregates that vector - sums the squares of even numbers
for (i in a) {
  if (i %% 2 == 0) total <- total + i * i
}

print (total)

```

```{r}
print("Assignment 2 Vector Edition")
my_vec <- 1:100
even_nums <- ifelse(my_vec %% 2 == 0, my_vec, NA)

even_nums
square <- function(element) {
  return(element * element);
}
even_nums <- sapply(even_nums, square)
even_nums
total <- sum(even_nums, na.rm = TRUE)
total
```

