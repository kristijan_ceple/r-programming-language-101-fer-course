---
title: "ZZV_3"
author: "KristijanCeple"
date: "10/29/2019"
output: html_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

## R Markdown

This is an R Markdown document. Markdown is a simple formatting syntax for authoring HTML, PDF, and MS Word documents. For more details on using R Markdown see <http://rmarkdown.rstudio.com>.

When you click the **Knit** button a document will be generated that includes both content as well as the output of any embedded R code chunks within the document. You can embed an R code chunk like this:

```{r cars}
summary(cars)
```

## Including Plots

You can also embed plots, for example:

```{r pressure, echo=FALSE}
plot(pressure)
```

Note that the `echo = FALSE` parameter was added to the code chunk to prevent printing of the R code that generated the plot.

# 1. Zadatak
```{r}
v1 <- 11:99
v2 <- integer(100)
v3 <- seq(0, 1, 0.1)

v1
v2
v3
```

# 2. Zadatak
```{r}
nums <- 101:1001
numsMod10 <- nums[nums%%10==0]

nums
numsMod10

numsMod10Sum <- sum(numsMod10)
numsMod10Sum
```

# 3. Zadatak

```{r}
set.seed(1234)
m <- matrix(c(sample(1:100, 9, T)), nrow = 3, ncol = 3, byrow = T)
m

determinant <- det(m)
if(determinant != 0) {
  I <- diag(1, nrow(m), ncol(m))     # Identity matrix
  # formula <- (A %*% AI = I)
  mi <- solve(m, I)
  print(mi)
  
  res <- m %*% mi
  res <- zapsmall(res)
  print(res)
  
  if(all(res == I)){
    print("Inverse calculation successful")
  } else{
    print("Inverse calculation unsuccessful")
  }
} else {
  print("Determinant is 0 -- inverse can't be calculated!")
}
```

# 4. Zadatak

```{r}
svastara = list(brojevi = 1:3, slova = list("A", "B"), c(T, F), imena = list("Ivo", "Ana"))
class(svastara[[2]])
svastara$slova[3]
nchar(svastara$imena)
length(svastara$imena)
append(svastara$imena, "Pero")
svastara$imena <- list(svastara$imena, "Pero")
4 %in% svastara[[1]]
3 %in% svastara[[1]]

vectBase = 1:3
toAdd = list(a = vectBase, b = vectBase, c = vectBase)
svastara <- append(svastara, toAdd)
svastara
```

